package com.example.demo.controllers;

import com.example.demo.TestUtils;
import com.example.demo.model.persistence.Cart;
import com.example.demo.model.persistence.Item;
import com.example.demo.model.persistence.UserOrder;
import com.example.demo.model.persistence.repositories.OrderRepository;
import com.example.demo.model.persistence.repositories.UserRepository;
import com.example.demo.model.persistence.User;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class OrderControllerTest {

    private OrderController orderController;

    private UserRepository userRepo = mock(UserRepository.class);

    private OrderRepository orderRepo = mock(OrderRepository.class);

    @Before
    public void SetUp(){
        orderController = new OrderController();
        TestUtils.injectObjects(orderController, "userRepository", userRepo);
        TestUtils.injectObjects(orderController, "orderRepository", orderRepo);
    }

    @Test
    public void submit(){
        String username = "Sofia";
        User user = new User();
        user.setUsername(username);
        Cart cart = new Cart();
        cart.setItems(new ArrayList<>());
        user.setCart(cart);

        when(userRepo.findByUsername(username)).thenReturn(user);

        ResponseEntity<UserOrder> submit = orderController.submit(username);

        assertNotNull(submit);
        assertEquals(200, submit.getStatusCodeValue());
    }

    @Test
    public void get_orders_for_user(){
        String username = "Sofia";
        User user = new User();
        user.setUsername(username);
        when(userRepo.findByUsername(username)).thenReturn(user);
        ResponseEntity<List<UserOrder>> ordersForUser = orderController.getOrdersForUser(username);
        assertNotNull(ordersForUser);
        assertEquals(200, ordersForUser.getStatusCodeValue());
    }


}
