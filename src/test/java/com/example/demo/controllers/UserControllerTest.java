package com.example.demo.controllers;

import com.example.demo.TestUtils;
import com.example.demo.model.persistence.User;
import com.example.demo.model.persistence.repositories.CartRepository;
import com.example.demo.model.persistence.repositories.UserRepository;
import com.example.demo.model.requests.CreateUserRequest;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class UserControllerTest {

    private UserController userController;

    private UserRepository userRepo = mock(UserRepository.class);

    private CartRepository cartRepo = mock(CartRepository.class);

    private BCryptPasswordEncoder encoder = mock(BCryptPasswordEncoder.class);

    @Before
    public void setUp(){
        userController = new UserController();
        TestUtils.injectObjects(userController, "userRepository", userRepo);
        TestUtils.injectObjects(userController, "cartRepository", cartRepo);
        TestUtils.injectObjects(userController, "bCryptPasswordEncoder", encoder);


    }

    @Test
    public void create_user_happy_path(){
        when(encoder.encode("testPassword")).thenReturn("thisIsHashed");
        CreateUserRequest r = new CreateUserRequest();
        r.setUsername("test");
        r.setPassword("testPassword");
        r.setConfirmPassword("testPassword");

        ResponseEntity<User> responce = userController.createUser(r);
        assertNotNull(responce);
        assertEquals(200, responce.getStatusCodeValue());

        User u = responce.getBody();
        assertNotNull(u);
        assertEquals(0, u.getId());
        assertEquals("test", u.getUsername());
        assertEquals("thisIsHashed", u.getPassword());
    }

    @Test
    public void find_user_by_name(){
        User user = new User();
        user.setUsername("test");
        user.setPassword("testPassword");
        when(userRepo.findByUsername("test")).thenReturn(user);
        ResponseEntity<User> byUserName = userController.findByUserName("test");
        assertNotNull(byUserName);
        assertEquals(200, byUserName.getStatusCodeValue());
        assertEquals("test", byUserName.getBody().getUsername());
    }

    @Test
    public void find_user_by_id(){
        User user = new User();
        user.setId(1);
        user.setUsername("test");
        user.setPassword("testPassword");
        when(userRepo.findById((long) 1)).thenReturn(java.util.Optional.of(user));
        ResponseEntity<User> byId = userController.findById(1L);
        assertNotNull(byId);
        assertEquals(200, byId.getStatusCodeValue());
        assertEquals(1, byId.getBody().getId());
    }

    @Test
    public void password_lenght(){
        when(encoder.encode("smal")).thenReturn("thisIsHashed");
        CreateUserRequest r = new CreateUserRequest();
        r.setUsername("test");
        r.setPassword("small");
        r.setConfirmPassword("small");

        ResponseEntity<User> responce = userController.createUser(r);
        assertNotNull(responce);
        assertNotEquals(200, responce.getStatusCodeValue());
    }

    @Test
    public void passwords_do_not_match(){
        when(encoder.encode("testPassword")).thenReturn("thisIsHashed");
        CreateUserRequest r = new CreateUserRequest();
        String password = "testPassword";
        String confirmPassword = "NotTestPassword";
        r.setUsername("test");
        r.setPassword(password);
        r.setConfirmPassword(confirmPassword);
        ResponseEntity<User> responce = userController.createUser(r);
        assertNotNull(responce);
        assertNotEquals(200, responce.getStatusCodeValue());
    }

    
}
