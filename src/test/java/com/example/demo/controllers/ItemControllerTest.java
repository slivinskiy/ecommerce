package com.example.demo.controllers;

import com.example.demo.TestUtils;
import com.example.demo.model.persistence.Item;
import com.example.demo.model.persistence.repositories.ItemRepository;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ItemControllerTest {

    private ItemController itemController;

    private ItemRepository itemRepository = mock(ItemRepository.class);

    @Before
    public void setUp(){
        itemController = new ItemController();
        TestUtils.injectObjects(itemController, "itemRepository", itemRepository);
    }

    @Test
    public void get_item_by_name(){
        Item item = new Item();
        item.setName("testItem");
        item.getDescription("justItem");
        List<Item> itemList = new ArrayList<Item>();
        itemList.add(item);
        when(itemRepository.findByName("testItem")).thenReturn(itemList);
        ResponseEntity<List<Item>> testItem = itemController.getItemsByName("testItem");
        assertNotNull(testItem);
        assertEquals(200, testItem.getStatusCodeValue());
        List<Item>  i = testItem.getBody();
        assertEquals("testItem", i.get(0).getName());
    }

    @Test
    public void get_item_by_id(){
        Item item = new Item();
        item.setName("testItem");
        item.setId(0L);
        when(itemRepository.findById(item.getId())).thenReturn(java.util.Optional.of(item));
        ResponseEntity<Item> itemById = itemController.getItemById(item.getId());
        assertNotNull(itemById);
        assertEquals(200, itemById.getStatusCodeValue());
        assertEquals(0, itemById.getBody().getId());
    }

    @Test
    public void get_items(){
        Item item1  = new Item();
        Item item2 = new Item();
        List<Item> items = new ArrayList<>();
        items.add(item1);
        items.add(item2);
        when(itemRepository.findAll()).thenReturn(items);
        ResponseEntity<List<Item>> getItems = itemController.getItems();
        assertNotNull(getItems);
        assertEquals(2, getItems.getBody().size());
    }



}
